#include <iostream>

using namespace std;

 void myswap(int &ptr1, int &ptr2)
{
  auto temp = ptr1;
  ptr1 = ptr2;
  ptr2 = temp;
  
  
}


int main()
{
  int a = 25, b = 11;  
  int *var;  
  
  //store pointer variables
		var = &a;
		var = &b;
  
		cout << "What is the value of a?" << endl ;
			cout << a << endl;
  
		cout << "What is the value of b?" << endl ;
 			 cout << b << endl;
  
		cout << "Print the var" << endl ;
			cout << var << endl;

		cout << "The value of *var is:" << endl ;
			cout << *var << endl;
	
  cout<<"What am I doing wrong ☹ \n\n";

  cout<<"a = "<<a<<", b = "<<b<<endl;

//swap(a,b); //why does theirs work????   
 
  myswap(a,b); // but mine doesn't ?!?!?!?!!!???

  cout<<"a = "<<a<<", b = "<<b<<endl;

	return 0;
}
